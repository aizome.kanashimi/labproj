import turtle

def Rysuj_prostokat():
    wn = turtle.Screen();
    wn.bgcolor("white")
    t = turtle.Turtle();
    t.pensize(3)
    t.pencolor("yellow")
    t.forward(100);
    t.right(90);
    t.forward(50);
    t.right(90);
    t.forward(100);
    t.right(90);
    t.forward(50);
    
'''Rysuj_prostokat'''

def Rysuj_kwadrat():
    wn = turtle.Screen();
    wn.bgcolor("white")
    a = turtle.Turtle();
    a.pensize(3)
    a.pencolor("yellow")
    tab = ["red", "blue", "yellow"]
    licznik = 0;
    while licznik < 4: 
        a.forward(100);
        a.right(90);
        a.pencolor(tab[licznik])
        licznik += 1

    
'''Rysuj_kwadrat()'''

def Rysuj_trojkat():
    wn = turtle.Screen();
    wn.bgcolor("white")
    b = turtle.Turtle();
    b.pensize(3)
    b.pencolor("yellow")
    tab = ["red", "blue", "green"]
    licznik = 0;
    while licznik < 3: 
        b.forward(100);
        b.right(120);
        b.pencolor(tab[licznik])
        licznik += 1

'''Rysuj_trojkat()'''


def Rysuj_okrag():
    wn = turtle.Screen();
    wn.bgcolor("white")
    c = turtle.Turtle();
    c.pencolor("red");
    c.circle(50);
    

'''Rysuj_okrag()'''

def Wybierz():
    numer = input("Wybierz figure: \n1 - prostokat \n2 - kwadrat"
                      "\n3 - trojkat \n4 - okrag \n")
    if (numer == 1):
                      Rysuj_prostokat()
    elif (numer == 2):
                       Rysuj_kwadrat()
    elif (numer == 3):
                      Rysuj_trojkat()
    elif (numer == 4):
                      Rysuj_okrag()
    else:
         print("Zla liczba")             

Wybierz()

